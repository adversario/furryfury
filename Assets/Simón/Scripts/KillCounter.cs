﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillCounter : MonoBehaviour
{

    public static int killCounter = 0;
    Text kills;

    void Start()
    {
        kills = GetComponent<Text>();
    }

    void Update()
    {
        kills.text = "O" + killCounter;
    }
}
