﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMeleeAttack : MonoBehaviour
{
    public LayerMask whatIsEnemies;

    [SerializeField]
    protected Transform hitPosition;
    [SerializeField]
    protected int hitDamage;
    [SerializeField]
    protected float hitRange;
    [SerializeField]
    protected float startTimeAttack;
    [SerializeField]
    protected float timeBtwAttack;

    void Update()
    {
        if (startTimeAttack <= 0)
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(hitPosition.position, hitRange, whatIsEnemies);

                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    enemiesToDamage[i].GetComponent<Enemy>().TakeDamage(hitDamage);
                }
            }

            startTimeAttack = timeBtwAttack;
        }

        else
        {
            startTimeAttack -= Time.deltaTime;
        }
    }
}
