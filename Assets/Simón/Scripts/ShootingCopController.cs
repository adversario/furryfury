﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingCopController : Enemy
{
    [Header("Movement")]
    public float speed;
    public float stoppingDistance, retreatDistance, firingDistance;

    [Header("Shooting Variables")]
    public Transform firePosition;
    public GameObject bulletPrefab;
    [SerializeField]
    protected float startTimeAttack;
    [SerializeField]
    protected float timeBtwAttack;

    Transform player;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }

        else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > retreatDistance)
        {
            var targetPosition = new Vector2(transform.position.x, player.position.y);

            transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
            
            if (startTimeAttack <= 0)
            {
                if (Mathf.Abs(transform.position.y - targetPosition.y) < firingDistance)
                {
                    if (!enemyAn.GetCurrentAnimatorStateInfo(0).IsName("Attacking")
                         && !enemyAn.GetCurrentAnimatorStateInfo(0).IsName("GetHit"))
                    {
                        Shoot();
                        enemyAn.SetTrigger("Attacking");
                    }

                }

                startTimeAttack = timeBtwAttack;
            }

            else
            {
                startTimeAttack -= Time.deltaTime;
            }
        }

        else if (Vector2.Distance(transform.position, player.position) < retreatDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
        }

        Vector2 toPlayer;

        toPlayer = (player.position - transform.position);

        if (toPlayer.x > 0)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        else if (toPlayer.x < 0)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        if (!enemyAn.GetCurrentAnimatorStateInfo(0).IsName("Attacking")
            && !enemyAn.GetCurrentAnimatorStateInfo(0).IsName("GetHit"))
        {
            enemyAn.SetBool("IsWalking", toPlayer.magnitude != 0);
        }
    }

    void Shoot()
    {
        Instantiate(bulletPrefab, firePosition.position, firePosition.rotation);
    }

}
