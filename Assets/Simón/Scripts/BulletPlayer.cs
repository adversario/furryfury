﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : MonoBehaviour
{
    public float speed;
    public int damage;
    public Rigidbody2D bulletRb;

    public Splatter splatter;
    
    
    void Start()
    {
        bulletRb.velocity = transform.right * speed;
        Destroy(gameObject, 15);
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Enemy enemy = hitInfo.GetComponent<Enemy>();
        if (enemy != null)
        {
            Instantiate(splatter, gameObject.transform.position, Quaternion.identity);
            enemy.TakeDamage(damage);
        }

        else if (hitInfo.GetComponent<BulletPlayer>())
        {
            return;
        }

        else if (hitInfo.GetComponent<BulletEnemy>())
        {
            return;
        }

        Destroy(gameObject);
    }
}
