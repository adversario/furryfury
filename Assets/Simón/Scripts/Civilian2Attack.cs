﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Civilian2Attack : MonoBehaviour
{
    public LayerMask whatIsEnemies;

    [SerializeField]
    protected Transform hitPosition;
    [SerializeField]
    protected int hitDamage;
    [SerializeField]
    protected float hitRange;
    [SerializeField]
    protected float startTimeAttack;
    [SerializeField]
    protected float timeBtwAttack;

    protected virtual void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(hitPosition.position, hitRange);
    }

    private void Update()
    {
        if (startTimeAttack <= 0)
        {
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(hitPosition.position, hitRange, whatIsEnemies);

            for (int i = 0; i < enemiesToDamage.Length; i++)
            {
                enemiesToDamage[i].GetComponent<PlayerController>().TakeDamage(hitDamage);
            }

            startTimeAttack = timeBtwAttack;
        }

        else
        {
            startTimeAttack -= Time.deltaTime;
        }
    }
}