﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    public float speed;
    public int damage;
    public Rigidbody2D bulletRb;
    
    void Start()
    {
        bulletRb.velocity = transform.right * speed;
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        PlayerController player = hitInfo.GetComponent<PlayerController>();
        if (player != null)
        {
            player.TakeDamage(damage);
        }

        else if (hitInfo.GetComponent<BulletEnemy>())
        {
            return;
        }

        else if (hitInfo.GetComponent<BulletPlayer>())
        {
            return;
        }

        Destroy(gameObject);
    }
}
