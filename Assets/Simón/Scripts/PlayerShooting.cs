﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public Transform firePosition;
    public GameObject bulletPrefab;
    [SerializeField]
    protected float startTimeAttack;
    [SerializeField]
    protected float timeBtwAttack;

    void Update()
    {
        if (startTimeAttack <= 0)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                Shoot();
            }

            startTimeAttack = timeBtwAttack;
        }

        else
        {
            startTimeAttack -= Time.deltaTime;
        }
    }

    void Shoot()
    {
        Instantiate(bulletPrefab, firePosition.position, firePosition.rotation);
    }
}
