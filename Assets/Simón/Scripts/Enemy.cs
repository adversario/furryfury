﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class Enemy : MonoBehaviour
{
    [SerializeField] public float health, maxHealth;

    protected Animator enemyAn;
    protected Rigidbody2D enemyRb;
    protected SpriteRenderer enemySr;

    void Awake()
    {
        enemyAn = GetComponent<Animator>();
        enemyRb = GetComponent<Rigidbody2D>();
        enemySr = GetComponent<SpriteRenderer>();
    }

    public void TakeDamage(int hitDamage)
    {
        health -= hitDamage;

        if (health <= 0)
        {
            KillCounter.killCounter += 1;
            Destroy(gameObject);
        }
    }
}
