﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Splatter : MonoBehaviour
{
    public List<Sprite> sprites; //ref to the sprites which will be used by sprites renderer
    public Color32 splatColor; //color values which can be assigned by another script
    private SpriteRenderer spriteRenderer;//ref to sprite renderer component

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Count)];
        ApplyStyle();
    }
    //this methode assign the color to the splatter
    public void ApplyStyle()
    {
        spriteRenderer.color = splatColor;
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
    }
}