﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeCopController : Enemy
{
    enum States { patrol, pursuit }

    States state = States.patrol;

    public float horizontalSpeed, verticalSpeed;

    [Header("Melee Attack Variables")]
    [SerializeField]
    float searchRange = 1;
    [SerializeField]
    float stoppingDistance = 1;

    Transform player;
    Vector3 target;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        InvokeRepeating("SetTarget", 0, 1); // Set target, for how long, how often it looks for a target
        InvokeRepeating("SendHit", 0, 1); // Attack, how long the attack lasts, how often it attacks
    }

    protected virtual void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, searchRange);
        Gizmos.DrawWireSphere(target, 10f);
    }

    void SetTarget()
    {
        if (state != States.patrol)
        {
            return;
        }

        target = new Vector2(transform.position.x + Random.Range(-searchRange, searchRange), // Searches randomly between set ranges on X
            Random.Range(-searchRange, searchRange)); // On Y
    }

    void SendHit()
    {
        if (state != States.pursuit)
        {
            return;
        }
        if (speed.magnitude != 0)
        {
            return;
        }
    }

    Vector2 speed;

    void Update()
    {

        if (state == States.pursuit) // Pursuit
        {
            target = player.transform.position;

            if (Vector3.Distance(target, transform.position) > searchRange * 1.2f) // 20% further from searchRange makes Cop go Patrol again
            {
                target = transform.position;
                state = States.patrol;
                return;
            }
        }

        else if (state == States.patrol) // Patrol
        {
            var range = Physics2D.CircleCast(transform.position, searchRange, Vector2.up);

            if (range.collider != null)
            {
                if (range.collider.CompareTag("Player")) // If true, starts Pursuit
                {
                    state = States.pursuit;
                    return;
                }
            }
        }

        speed = target - transform.position; // Creates vector to go against Player

        if (speed.x > 0)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        else if (speed.x < 0)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        if (speed.magnitude < stoppingDistance) // Makes enemy stop before hitting Player
        {
            speed = Vector2.zero;
        }

        speed.Normalize();

        if (!enemyAn.GetCurrentAnimatorStateInfo(0).IsName("Attacking"))
        {
            enemyAn.SetBool("IsWalking", speed.magnitude != 0);
        }

        enemyRb.velocity = new Vector2(speed.x * horizontalSpeed, speed.y * verticalSpeed); // Movement
    }
}
