﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class PlayerController : MonoBehaviour
{
    public float health, maxHealth;
    public float horizontalSpeed, verticalSpeed;
    Vector2 control;

    protected Animator playerAn;
    protected Rigidbody2D playerRb;
    protected SpriteRenderer playerSr;

    void Awake()
    {
        playerAn = GetComponent<Animator>();
        playerRb = GetComponent<Rigidbody2D>();
        playerSr = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate()
    {
        // Movement
        control = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        playerRb.velocity = new Vector2(control.x * horizontalSpeed, control.y * verticalSpeed);

        // Flip X Sprite
        if (control.x > 0)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        else if (control.x < 0)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        // Animations

        if (!playerAn.GetCurrentAnimatorStateInfo(0).IsName("Scratch"))
        {
            playerAn.SetBool("IsWalking", control.magnitude != 0);
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
            playerAn.SetTrigger("Scratch");
        }
    }

    public void TakeDamage(int hitDamage)
    {
        health -= hitDamage;

        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

}
