﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    public List<Enemy> enemies = new List<Enemy>();

    bool AreEnemiesDead()
    {
        var result = true;
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] != null && enemies[i].health > 0)
            {
                result = false;
            }
        }
        return result;
    }

    void Update()
    {
        if (AreEnemiesDead())
        {
            Destroy(gameObject);
        }
    }
}
