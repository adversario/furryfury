﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    Vector3 localScale;
    public PlayerController player;

    void Start()
    {
        localScale = transform.localScale;
    }

    void Update()
    {
        localScale.x = player.health / player.maxHealth;
        transform.localScale = localScale;
    }
}